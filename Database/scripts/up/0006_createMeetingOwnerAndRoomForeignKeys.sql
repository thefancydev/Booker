ALTER TABLE [dbo].[Meeting]
ADD CONSTRAINT [FK_Meeting_Owner_Id] FOREIGN KEY ([Owner_Id]) REFERENCES [dbo].[User](Id)
GO

ALTER TABLE [dbo].[Meeting]
ADD CONSTRAINT [FK_Meeting_Room_Id] FOREIGN KEY ([Room_Id]) REFERENCES [dbo].[Room](Id)
GO