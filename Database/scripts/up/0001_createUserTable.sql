CREATE 
TABLE [dbo].[User]
([Id] INT IDENTITY(1,1) NOT NULL,
[UserName] NVARCHAR(100) NOT NULL,
[PasswordHash] NVARCHAR(MAX) NOT NULL,
[FirstName] NVARCHAR(100) NOT NULL,
[LastName] NVARCHAR(100) NOT NULL,
[IsAdmin] BIT DEFAULT 0 NOT NULL,
CONSTRAINT [PK_User_Id] PRIMARY KEY ([Id]))
GO
