ALTER TABLE [dbo].[MeetingAttendees]
ADD CONSTRAINT [FK_Meeting_Attendee_Id] FOREIGN KEY ([User_Id]) REFERENCES [dbo].[User](Id)
GO

ALTER TABLE [dbo].[MeetingAttendees]
ADD CONSTRAINT [FK_Meeting_Id] FOREIGN KEY ([Meeting_Id]) REFERENCES [dbo].[Meeting](Id)
GO