CREATE
TABLE [dbo].[MeetingAttendees]
([User_Id] INT NOT NULL,
[Meeting_Id] INT NOT NULL,
CONSTRAINT [CPK_MeetingAttendees] PRIMARY KEY ([User_Id], [Meeting_Id]))
GO