﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using Booker.Data.Ops;
using static Booker.Data.Session;
using System.Linq;

namespace Booker.Data.Types
{
    public class Meeting : Ops.Data
    {
        public Meeting()
        {
            this.Attendees = new List<User>();
        }

        public Meeting(string title, string description, DateTime start, DateTime end, User owner)
        {
            this.Title = title;
            this.Description = description;
            this.StartTime = start;
            this.EndTime = end;
            this.Owner = owner;

            this.Attendees = new List<User>(); //required by fluent
        }
        public virtual int Id { get; protected set; }

        public virtual string Title { get; set; }
        public virtual string Description { get; set; }

        public virtual DateTime StartTime { get; protected set; }
        public virtual DateTime EndTime { get; protected set; }
        public virtual TimeSpan Length => this.EndTime - this.StartTime;

        public virtual User Owner { get; protected set; }
        public virtual IList<User> Attendees { get; set; }

        public virtual Room MeetingRoom { get; set; }

        public static List<Meeting> GetAll()
        {
            using (var session = GetSession())
            {
                return session.QueryOver<Meeting>()
                    .List()
                    .ToList();
            }
        }

        public static Meeting GetById(int id)
        {
            using (var session = GetSession())
            {
                return session.QueryOver<Meeting>()
                    .Where(x => x.Id == id)
                    .SingleOrDefault();
            }
        }
    }

    public class MeetingMap : ClassMap<Meeting>
    {
        public MeetingMap()
        {
            Table("[dbo].[Meeting]");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();

            Map(x => x.Title).Not.Nullable();
            Map(x => x.Description).Not.Nullable();

            Map(x => x.StartTime).Not.Nullable();
            Map(x => x.EndTime).Not.Nullable();

            References(x => x.Owner).Column("[Owner_Id]").Not.LazyLoad().Not.Nullable();

            References(x => x.MeetingRoom).Column("[Room_Id]").Not.LazyLoad().Not.Nullable();

            HasManyToMany(x => x.Attendees).Table("[dbo].[MeetingAttendees]").ParentKeyColumn("[Meeting_Id]").ChildKeyColumn("[User_Id]");
        }
    }
}
