﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Booker.Data.Session;
using Booker.Data.Ops;

namespace Booker.Data.Types
{
    public class User : Ops.Data
    {
        public User()
        {
            this.OwnedMeetings =
                this.AttendingMeetings =
                new List<Meeting>();
        }

        public User(string firstname, string lastname, string username, string password)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Username = username;
            this.PasswordHash = Security.HashPassword(password);

            this.OwnedMeetings = 
                this.AttendingMeetings = 
                new List<Meeting>();
        }

        public virtual int Id { get; protected set; }

        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public virtual string Username { get; protected set; }
        public virtual string PasswordHash { get; protected set; }

        public virtual IList<Meeting> OwnedMeetings { get; set; }
        public virtual IList<Meeting> AttendingMeetings { get; protected set; } //protected because we won't edit this

		public virtual bool IsAdministrator { get; protected set; }

		public virtual void SaveOrUpdate()
		{
			using (var session = GetSession())
			{
				session.SaveOrUpdate(this);
			}
		}

		public static List<User> GetAll()
		{
			using (var session = GetSession())
			{
				return session.QueryOver<User>()
					.List()
					.ToList();
			}
		}

        public static User GetByDetails(int? Id = null, string Username = null) //search params are mutually exclusive, so both must be nullable
		{
            using (var session = GetSession())
            {
				if (Id != null && Username == null) //obviously the search params are mutually exclusive
					return session.QueryOver<User>()
						.Where(x => x.Id == Id)
						.SingleOrDefault(); //if user doesn't exist null will be returned anyway
				else if (Username != null && Id == null)
					return session.QueryOver<User>()
						.Where(x => x.Username == Username)
						.SingleOrDefault();
				else if (Username == null && Id == null)
					throw new KeyNotFoundException("A username or Id is required");
				else
					throw new InvalidOperationException("Something happened... not my fault... *walks out door quickly*");
            }
        }
    }

    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("[dbo].[User]");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();

            Map(x => x.FirstName).Not.Nullable();
            Map(x => x.LastName).Not.Nullable();

            Map(x => x.Username).Not.Nullable();
            Map(x => x.PasswordHash).Not.Nullable();

			Map(x => x.IsAdministrator).Not.Nullable();

            HasMany(x => x.OwnedMeetings).KeyColumn("Owner_Id").Not.LazyLoad();

            HasManyToMany(x => x.AttendingMeetings).Table("[dbo].[MeetingAttendees]").ParentKeyColumn("[User_Id]").ChildKeyColumn("[Meeting_Id]");

        }
    }
}
