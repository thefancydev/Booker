﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Booker.Data.Ops;
using static Booker.Data.Session;

namespace Booker.Data.Types
{
    public class Room : Ops.Data
    {
        public Room()
        {
            this.Meetings = new List<Meeting>();
        }

        public Room(string name)
        {
            this.Name = name;
            this.Meetings = new List<Meeting>();
        }

        public virtual int Id { get; protected set; }

        public virtual string Name { get; set; }

        public virtual IList<Meeting> Meetings { get; set; }

        public static List<Room> GetAll()
        {
            using (var session = GetSession())
            {
                return session.QueryOver<Room>()
                    .List()
                    .ToList();
            }
        }

        public static Room GetById(int id)
        {
            using (var session = GetSession())
            {
                return session.QueryOver<Room>()
                    .Where(x => x.Id == id)
                    .SingleOrDefault();
            }
        }
    }

    public class RoomMap : ClassMap<Room>
    {
        public RoomMap()
        {
            Table("[dbo].[Room]");
            LazyLoad();

            Id(x => x.Id).Unique().Not.Nullable();

            Map(x => x.Name).Not.Nullable();

            HasMany(x => x.Meetings).KeyColumn("Room_Id").Not.LazyLoad().Not.LazyLoad();
        }
    }
}
