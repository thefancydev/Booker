﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booker.Data.Exceptions
{
	public class EverythingsBrokenException : Exception //this is just for an easter egg, but I wanted to put it in the proper place. I don't actually have custom exceptions in this app
	{
		public EverythingsBrokenException() : base() { }

		public EverythingsBrokenException(string message) : base(message) { }

		public EverythingsBrokenException(string message, Exception innerException) : base(message, innerException) { }
	}
}
