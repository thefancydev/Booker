﻿using System;
using System.Collections.Generic;
using System.Text;
using static Booker.Data.Session;

namespace Booker.Data.Ops
{
    public class Data
    {
        public virtual void Insert()
        {
            using (var session = GetSession())
            {
                session.Save(this);
            }
        }

        public virtual void Update()
        {
            using (var session = GetSession())
            {
                session.Update(this);
            }
        }

        public virtual void Delete()
        {
            using (var session = GetSession())
            {
                session.Delete(this);
            }
        }
    }
}
