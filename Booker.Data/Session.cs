﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booker.Data
{
    public static class Session
    {
        private static ISessionFactory SessionFactory()
        {
            return Fluently.Configure()
                           .Database(MsSqlConfiguration.MsSql7
                                                       .ConnectionString(s =>
                                                       {
#if DEBUG
                                                           s.Server(".\\SQLEXPRESS");
                                                           s.Database("Booker.DB");
                                                           s.TrustedConnection();
#else
                                                           //any prod DB details would go here
#endif
                                                       }))
                           .Mappings(m => m.FluentMappings
                                           .AddFromAssemblyOf<Types.UserMap>()
                                           .AddFromAssemblyOf<Types.MeetingMap>()
                                           .AddFromAssemblyOf<Types.RoomMap>())
                           .BuildConfiguration()
                           .BuildSessionFactory();
        }

        public static ISession GetSession() => SessionFactory().OpenSession();

        public static IStatelessSession GetStatelessSession() => SessionFactory().OpenStatelessSession();
    }
}
