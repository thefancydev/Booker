﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booker.Web.ActionFilters
{
	public class AuthRequiredActionFilter : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			if (!Auth.IsLoggedIn)
			{
				filterContext.Controller.ViewData["error"] = "THOU SHALT NOT PASS! (You must be logged in to view this)";
				UrlHelper u = new UrlHelper(filterContext.RequestContext);

				filterContext.Result = new RedirectResult(u.Action("Index", "Home"));
			}
		}
	}
}

