﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Booker.Data.Types;

namespace Booker.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
			var meetings = new List<Meeting>();
			if (Auth.IsLoggedIn)
			{
				meetings = Auth.AuthorizedAs.OwnedMeetings.ToList();
			}
			else
			{
				ViewData["message"] = "Please login to view your own meetings";
				meetings = Data.Types.Meeting.GetAll();
			}

            return View(meetings);
        }

        [Route("Meeting/{Id}")]
        public ActionResult Meeting(int Id)
        {
            var mod = Data.Types.Meeting.GetById(Id);

            return View(Data.Types.Meeting.GetById(Id));
        }

        [Route("Users/")]
        public ActionResult Accounts()
        {
            var users = Data.Types.User.GetAll();
            var mod = new List<User>();
            foreach(var user in users)
            {
                mod.Add(user);
            }

            return View(mod);
        }

        [Route("User/{Id}")]
        public ActionResult Account(int Id)
        {
            var mod = Data.Types.User.GetByDetails(Id: Id);

            return View(mod);
        }
    }
}