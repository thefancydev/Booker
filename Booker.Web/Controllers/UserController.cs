﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Booker.Data.Types;
using Booker.Web.ActionFilters;

namespace Booker.Web.Controllers
{
    public class UserController : Controller
    {
		[AuthRequiredActionFilter, Route("/User/Me")]
		public ActionResult Index()
		{
			//this doesn't need an auth check due to the action filter
			return View(Auth.AuthorizedAs);
		}

		[AuthRequiredActionFilter, Route("/Users")] //user accounts are kinda sensitive, so we want an auth filter here
		public ActionResult AllUsers()
		{
			return View(Data.Types.User.GetAll());
		}

		[Route("/User/LoginRegister")]
		public ActionResult LoginRegister()
		{
			if (Auth.IsLoggedIn)
			{
				ViewData["error"] = "You're already logged in. Logout first.";
				return RedirectToAction("Index", "Home");
			}
			else
			{
				return View();
			}
		}

		public ActionResult RegisterUser(string Firstname, string Lastname, string Username, string Password)
		{
			var newUser = new User(Firstname, Lastname, Username, Password);
			Auth.RegisterUser(newUser);

			ViewData["message"] = "Registration successful. Welcome!";

			return RedirectToAction("Index", "Home");
		}

        public ActionResult CheckCreds(string Username, string Password)
        {
			if (Auth.CheckCreds(Username, Password))
			{
				//bada bing, bada boom
				return RedirectToAction("Index", "Home"); 
			}
			ViewData["error"] = "Username or Password incorrect";
            return RedirectToAction("Index");
        }
    }
}