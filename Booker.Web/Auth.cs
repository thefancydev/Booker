﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Booker.Data.Types;
using Booker.Data.Ops;

namespace Booker.Web
{
    public static class Auth //obviously this is a bad way to do auth, but I needed to use my own DB and this was quickest. If it was a proper project, I would use stored procs with client side hashing and cookies, but hey ho
    {
		public static void RegisterUser(User user) //user is contructed in User/RegisterUser controller
		{
			//I've stuck this here to enable setting AuthorizedAs without requerying the DB on register (this is instead of calling checkCreds on the new user)
			user.SaveOrUpdate(); //all that's needed
			AuthorizedAs = user;
		}

        public static User AuthorizedAs { get; private set; }

        public static bool IsLoggedIn => AuthorizedAs == null ? false : true; //false if true, and true if false. My brain is a puddle of goo and mess right now

        public static bool CheckCreds(string username, string passwordHash)
        {
			var user = User.GetByDetails(Username: username);

			if (Data.Security.ValidatePassword(passwordHash, user.PasswordHash))
			{
				AuthorizedAs = user;
				return true;
			}
			else
			{
				return false; //this user is as false as my grandmother's teeth. *insert drum rimshot here*
			}
        }
    }
}